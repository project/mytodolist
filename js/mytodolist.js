(function ($, Drupal, drupalSettings) { 
  //Drupal behaviours
  Drupal.behaviors.mytodolist = {
    attach: function (context, settings) {
      // Add new item in todolist
      $('.addnewtodoelement', context).click(function(){
        var todoName = document.getElementById("todolistName").value;	
        var todoDate = document.getElementById("todolistDate").value;
        if (todoName == '') {
          alert("Please enter To-Do name.");
        } else {
          $('.addnewtodoelement').html('Adding');
          $.ajax({
            url: Drupal.url('addmytodotask'),
            data: {'todolisttext': todoName,todolistdate:todoDate},
            type: "POST",
            dataType: "HTML",
            success: function(response){
              if(response){
                $('#todolistcontentlisting').html(response);
                Drupal.attachBehaviors($('#todolistcontentlisting').get(0));
                document.getElementById("todolistName").value = "";					
                document.getElementById("todolistDate").value = "";
                $('.addnewtodoelement').html('+');
              }	else {
                alert('Task has not been created in db');
              }	
            },
            error: function(e){
              console.log(e);
            }
          });
        }
      });  

      // Add Strikeout	
      $(document).on("change","input:checkbox", function(){
        var classObj = $(this).attr('class');
        if(classObj == "todolist-class"){
          var id = $(this).attr('id');
          if($(this).is(":checked")) {
            $('#todolistlistcheckboxes').find('#todolist_'+id).addClass('strike-out');
            $.ajax({
              url: Drupal.url('completemytodo'),
              data: {'post_id': id,check_type:'1'},
              type: "POST",
              success: function(response){
              },
              error: function(e){
                console.log(e);
              }
            });			
          } else {
            $('#todolistlistcheckboxes').find('#todolist_'+id).removeClass("strike-out");
            $.ajax({
              url: Drupal.url('completemytodo'),
              data: {'post_id': id,check_type:'0'},
              type: "POST",
              success: function(response){
              },
              error: function(e){
                console.log(e);
              }
            });		
          }
        }
      });

      // Delete todolist items
      $('.deletetodoelements', context).click(function(){
        var checkedElements = [];		
        $('#todolistlistcheckboxes input:checked').each(function() {
          checkedElements.push($(this).attr('id'));
        });
        if(checkedElements.length > 0){
          $('.deletetodoelements').html('Deleting');
          $.ajax({
            url: Drupal.url('deletemytodo'),
            data: {'post_id': checkedElements},
            type: "POST",
            dataType: "HTML",
            success: function(response){
              if(response){
                $('#todolistcontentlisting').html(response);
                Drupal.attachBehaviors($('#todolistcontentlisting').get(0));
                $('.deletetodoelements').html('Delete');
              }	else {
                console.log('Todolist element deletion has some error!');
              }	
            },
            error: function(e){
              console.log(e);
            }
          });
        } else {
          alert('Please select at least one todolist checkbox.');
        }  
      });  

    }
  };
})(jQuery, Drupal, drupalSettings);