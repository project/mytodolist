<?php

namespace Drupal\mytodolist\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'My Todo List' Block.
 *
 * @Block(
 *   id = "mytodolist_block",
 *   admin_label = @Translation("My Todo List"),
 *   category = @Translation("My Todo List"),
 * )
 */
class MyTodoListBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Class Resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Constructor for MyTodoList Block.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param mixed $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   The class resolver service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    ClassResolverInterface $class_resolver
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->classResolver = $class_resolver;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('class_resolver')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    // Gets and Instance of the Controller and returns it's output.
    $controller = $this->classResolver->getInstanceFromDefinition('Drupal\mytodolist\Controller\MytodolistController');
    return $controller->mytodolist();
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheMaxAge() {
    // Stops the block from caching data.
    return 0;
  }

}
