<?php

namespace Drupal\mytodolist\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\mytodolist\Services\MytodolistService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Cache\Cache;

use Drupal\Core\Datetime\DrupalDateTime;

/**
 * MytodolistController class
 */
class MytodolistController extends ControllerBase {
  /**
   * Include the mytodlist service.
   *
   * @var \Drupal\mytodolist\Services\MytodolistService
   */
  protected $mytodolistService;
  /**
   * Constructor.
   */
  public function __construct(MytodolistService $mytodolist_service) {
    $this->mytodolistService = $mytodolist_service;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('mytodolist.helper')
    );
  }
  /**
   * Mytodolist
   */
  public function mytodolist() {
    $todolists = $this->mytodolistService->getTodolistData();
     return array(
      '#theme' => 'mytodolist',
      '#task_lists' => $todolists,
      '#cache' => [
        'contexts' => [ 
          'user', 
        ],
      ],
    );
  }
  /**
   * Add new todolist item
   */
  public function addmytodotask(Request $request) {
    $todoname = $request->request->get('todolisttext');
    $tododate = $request->request->get('todolistdate');
    $user = \Drupal::currentUser();
    $timezone = $user->timezone;
    $date = new DrupalDateTime($tododate);
    $raw_date = $date->getTimestamp();
    $tododate = \Drupal::service('date.formatter')->format($raw_date, 'custom', 'd-m-Y', $timezone);
    $data =[
      'todoname' => $todoname, 
      'tododate' => isset($tododate) ? $tododate : 0,
    ];
    $todolists = $this->mytodolistService->insertTodolistTask($data);
    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate(drupal_get_path('module','mytodolist').'/templates/mytodolistajax.html.twig');
    $response = $template->render(['task_lists' => $todolists]);
    return new Response($response);
  }  
  /**
   * Complete todolist task
   */
  public function completemytodo(Request $request) {
    $id = $request->request->get('post_id');
    $check_type = $request->request->get('check_type');
    $todolist = $this->mytodolistService->completemytodo($id,$check_type);
    return new Response($todolist);
  }  
  /**
   * Delete todolist items
   */
  public function deletemytodo(Request $request) {
    $ids = $request->request->get('post_id');
    foreach($ids as $id) {
      $this->mytodolistService->deleteTodolistTask($id);
    }
    $todolists = $this->mytodolistService->getTodolistData();
    $twig = \Drupal::service('twig');
    $template = $twig->loadTemplate(drupal_get_path('module','mytodolist').'/templates/mytodolistajax.html.twig');
    $response = $template->render(['task_lists' => $todolists]);
    return new Response($response);
  }    
}