<?php

namespace Drupal\mytodolist\Services;

use Drupal\Core\Database\Connection;

/**
 * MytodolistService class.
 */
class MytodolistService { 
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;
  /**
   * Construct a todolist object.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   */
  public function __construct(Connection $connection) {
    $this->connection = $connection;
  }  
  /**
   * Return todolist
   */
  public function getTodolistData() {
    $currentUserId = \Drupal::currentUser()->id();
    $select = $this->connection->select('mytodolist', 'todo');
    $select->fields('todo');
    $select->condition('created_by', $currentUserId, '=');
    $select->orderBy('id', 'DESC');
    $results = $select->execute()->fetchAll(\PDO::FETCH_ASSOC);
    return $results;
  }
  /**
   * Insert todolist task
   */
  public function insertTodolistTask($data=[]) {
    $currentUserId = \Drupal::currentUser()->id();
    if(!empty($data)) {
      $todod = strtotime($data['tododate']);
      $tododate = isset($data['tododate']) ? $todod : 0;
      $insert = $this->connection->insert('mytodolist')
      ->fields([
        'todoname' => $data['todoname'],
        'tododate'=> (int) $tododate,
        'status' => 1,
        'created_by' => $currentUserId,
        'updated_by' => $currentUserId,
        'created_at' => \Drupal::time()->getRequestTime(),
        'updated_at' => \Drupal::time()->getRequestTime(),
      ])
      ->execute();
    }
    $todolist = $this->getTodolistData();
    return $todolist;
  } 
  /**
   * Update my todolist
   */
	public function completemytodo($id,$check_type) {
    $currentUserId = \Drupal::currentUser()->id();
		if($check_type == "1") {
      $update = $this->connection->update('mytodolist')
      ->fields([
        'status' => 0,
        'updated_by' => $currentUserId,
        'updated_at' => \Drupal::time()->getRequestTime(),
      ])
      ->condition('id', $id, '=')
      ->execute();
			return '0';					
		} else if($check_type == "0"){
      $update = $this->connection->update('mytodolist')
      ->fields([
        'status' => 1,
        'updated_by' => $currentUserId,
        'updated_at' => \Drupal::time()->getRequestTime(),
      ])
      ->condition('id', $id, '=')
      ->execute();
			return '1';					
		}	else {
			return 'error';
		}							
  }	
  /**
   * Delete my todolist
   */  
  public function deleteTodolistTask($id) {
    if(isset($id)) {
      $deleted = $this->connection->delete('mytodolist')
      ->condition('id', $id)
      ->execute();
    }
    return true;
  }
}
